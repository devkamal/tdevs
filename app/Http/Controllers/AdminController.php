<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(){
        return view('admin.layouts.login');
    }

    public function AdminDashboard(){
        return view('admin.layouts.index');
    }

    public function AdminDestroy(){
        Auth::guard('web')->logout();
        return redirect()->route('login_form')->with('success','Logout Success');
    }

    public function viewAllUser(){
        if ($request->ajax()) {
            $data = User::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
       
                            $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
      
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
          
        return view('admin.layouts.index');
 
    }


    public function editUser($id){
        $editData = User::find($id);
        return view('admin.edit_user',compact('editData'));
    }

    public function update(Request $request, $id)
    {
        $data = User::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->update();

        $notification = array(
            'message' => 'User Update successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('admin_dashboard')->with($notification);
    }

    public function destroy($id)
    {
        User::find($id)->delete();

        $notification = array(
            'message' => 'User deleted success',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }



}
