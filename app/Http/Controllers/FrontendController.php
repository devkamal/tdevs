<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingRoomList;
use App\Models\Room;
use App\Models\RoomBookedDate;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{


    public function BookingSearch(Request $request)
    {
      $request->flash();

      if ($request->check_in == $request->check_out){
          $notification = array(
              'message' => 'Something want to wrong.',
              'alert-type' => 'error'
          );
          return redirect()->back()->with($notification);
      }


      $sdate = date('Y-m-d',strtotime($request->check_in));
      $edate = date('Y-m-d',strtotime($request->check_out));
      $edate = Carbon::create($edate)->subDay();
      $d_period = CarbonPeriod::create($sdate, $edate);
      $dt_array = [];
      foreach ($d_period as $period){
          array_push($dt_array, date('Y-m-d', strtotime($period)));
      }

      $check_date_booking_ids = RoomBookedDate::whereIn('book_date', $dt_array)->distinct()->pluck('booking_id')->toArray();


      $rooms = Room::withCount('room_numbers')->where('status', 1)->get();

//        $rm = Room::find(2);
//        $bookings = Booking::withCount('assign_rooms')->whereIn('id', $check_date_booking_ids)->where('room_id', $rm->id)->get()->toArray();
//        $total_book_room = array_sum(array_column($bookings, 'assign_rooms_count'));

      return view('frontend.single_page.room.rompage', compact('rooms', 'check_date_booking_ids'));
    }

    
}
