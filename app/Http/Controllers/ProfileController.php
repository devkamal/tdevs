<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ProfileUpdateRequest;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(Request $request)
    {
        $id   = Auth::user()->id;
        $data = User::find($id);
        $data->username = $request->username;
        $data->phone    = $request->phone;
        $data->email    = $request->email;
        $data->address  = $request->address;
        if($request->file('image')){
            $file = $request->file('image');
            @unlink(public_path('upload/userimg/'.$data->image));
            $filename = date('YdmHi').$file->getClientOriginalName();
            $file->move(public_path('upload/userimg'),$filename);
            $data['image'] = $filename;
        }
        $data->save();
        
        $notification = array(
            'message' => 'Profile Update Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function changePassword(){
        $id = Auth::user()->id;
        $userData = User::find($id);
        return view('frontend.userprofile.change_password',compact('userData'));
    }

    public function updateUserPassword(Request $request){
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

        $hashPassword = Auth::user()->password;
        if(Hash::check($request->oldpassword,$hashPassword)){
            $users = User::find(Auth::id());
            $users->password = bcrypt($request->new_password);
            $users->save();

            $notification = array(
                'message' => 'Password Update Successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('login')->with($notification);
        }else{
            session()->flash('message','Oops something went wrong!');
            return redirect('logout');
        }
    }

    public function UserLogout(Request $request){
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

         $notification = array(
            'message' => 'User Logout Successfully',
            'alert-type' => 'success'
        );

        return redirect('/login')->with($notification);
    } // End Mehtod 

}
