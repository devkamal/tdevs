<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\RoomType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        User::create([
            'name'      => 'md. kamal hossen',
            'username'  => 'kamal',
            'email'     => 'kamal@gmail.com',
            'password'  => Hash::make('12345678'),
            'role'      => 'admin',
            'status'    => 'active',
        ]);

        //user
        User::create([
            'name'      => 'tdevs',
            'username'  => 'narsingdi',
            'email'     => 'tdevs@gmail.com',
            'password'  => Hash::make('12345678'),
            'role'      => 'user',
            'status'    => 'active',
        ]);

        User::factory()->count(10)->create();

    }
}
