@extends('admin.layouts.master')
@section('content')

<div class="container">
      <div class="row">
            <div class="col-md-12">
                  <div class="card" style="margin:50px">
                        <form action="{{ route('update_user',$editData->id) }}" method="post">
                              @csrf

                              <div class="col-md-12 mx-2">
                                    <label for="name">name:</label>
                                    <input type="text" name="name" class="form-control" value="{{ $editData->name }}">
                                    @error('name')
                                    <span style="color:white"> {{ $message }} </span>
                                    @enderror
                              </div>

                              <div class="col-md-12 mx-2">
                                    <label for="email">email:</label>
                                    <input type="text" name="email" class="form-control" value="{{ $editData->email }}">
                                    @error('email')
                                    <span style="color:red"> {{ $message }} </span>
                                    @enderror
                              </div>

                           
                              <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="{{route('admin_dashboard')}}" type="submit" class="btn btn-info">Cancel</a>
                              </div>

                        </form>
                  </div>
            </div>
      </div>
</div>

@endsection