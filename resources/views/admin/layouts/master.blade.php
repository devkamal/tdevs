
<!DOCTYPE html>

<html
  lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default" 
  data-assets-path="../assets/" data-template="vertical-menu-template-free">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>

    <title>Dashboard - Analytics | Sneat - Bootstrap 5 HTML Admin Template - Pro</title>

    <meta name="description" content="" />

      @include('admin.includes.css')
      @stack('css')
  </head>

  <body>

      @include('admin.layouts.left_sidebar')

      @include('admin.layouts.navbar')

      @yield('content')
        
      @include('admin.layouts.footer')

      @include('admin.includes.js')
      @stack('js')
</body>
</html> 