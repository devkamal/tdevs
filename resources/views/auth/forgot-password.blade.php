

<!-- Contact section start -->
<div class="contact-section">
    <div class="container-fluid">
        <div class="row login-box">
            <div class="col-lg-6 align-self-center pad-0 form-section">
                <div class="form-inner">
                 
                    <h3>Recover Your Password</h3>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <!-- Email Address -->
                        <div class="form-group form-box">
                            <input type="email" name="email" class="form-control" placeholder="Email Address">
                        </div>

                        <div class="flex items-center justify-end mt-4">
                        <button type="submit" class=" btn btn-primary btn-block">Send Me Email</button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <p>Don't have an account? <a href="{{ route('register') }}" class="thembo"> Register here</a></p>
                </div>
            </div>
       
        </div>
    </div>
</div>
<!-- Contact section end -->

