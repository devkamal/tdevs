<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="container">
   <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
         <div class="container-xxl">
            <div class="authentication-wrapper authentication-basic container-p-y">
               <div class="authentication-inner">
                  <!-- Register -->
                  <div class="card">
                     <div class="card-body">
                        <!-- Logo -->
                        <!-- /Logo -->
                        <h4 class="mb-2">Welcome to Tdevs! 👋</h4>
                        <p class="mb-4">Please sign-in to you tdevs.</p>
                        <form method="POST" action="{{ route('login') }}">
                           @csrf
                           <div class="mb-3">
                              <label for="email" class="form-label">Email or Username</label>
                              <input
                                 type="text"
                                 class="form-control"
                                 id="email"
                                 name="email"
                                 placeholder="Enter your email or username"
                                 autofocus
                                 />
                           </div>
                           <div class="mb-3 form-password-toggle">
                              <div class="d-flex justify-content-between">
                                 <label class="form-label" for="password">Password</label>
                                 <a href="{{ route('password.request') }}">
                                 <small>Forgot Password?</small>
                                 </a>
                              </div>
                              <div class="input-group input-group-merge">
                                 <input
                                    type="password"
                                    id="password"
                                    class="form-control"
                                    name="password"
                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                    aria-describedby="password"
                                    />
                                 <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                              </div>
                           </div>
                           <div class="mb-3">
                              <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
                           </div>
                        </form>
                        <p class="text-center">
                           <span>New on our platform?</span>
                           <a href="{{ route('register') }}">
                           <span>Create an account</span>
                           </a>
                        </p>
                     </div>
                  </div>
                  <!-- /Register -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>