@extends('frontend.layouts.master')
@section('content')

<div class="user-page content-area-7 submit-property" style="margin:50px;">
    <div class="container">
        <div class="row">
      <!-- left wrapper start -->
      <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="user-profile-box mrb">
                  <!--header -->
                  <div class="header clearfix text-center">
                        <img src="{{ (!empty($userData->image))?url('upload/userimg/'.$userData->image):url('upload/noimg.png') }}"
                         alt="avatar" class="img-fluid profile-img" width="150px;" style="border-radius:50%">
                        <h2>{{ $userData->name }}</h2>
                  </div>
                  <!-- Detail -->
                  <div class="detail clearfix">
                        <ul>
                        
                           <li>
                              <a href="{{ route('user.logout') }}" class="border-bto2">
                                    <i class="flaticon-logout"></i>Log Out
                              </a>
                           </li>
                        </ul>
                  </div>
               </div>
            </div>
      <!-- left wrapper end -->
      <!-- middle wrapper start -->
  
</div>
   </div>
   </div>
@endsection