@extends('frontend.layouts.master')
@section('content')

<div class="user-page content-area-7 submit-property" style="margin:50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="user-profile-box mrb">
                  <!--header -->
                  <div class="header clearfix text-center">
                        <img src="{{ (!empty($userData->image))?url('upload/userimg/'.$userData->image):url('upload/noimg.png') }}"
                         alt="avatar" class="img-fluid profile-img" width="150px;" style="border-radius:50%">
                         <h2>{{ Auth::user()->name }}</h2>
                  </div>
                  <!-- Detail -->
                  <div class="detail clearfix">
                        <ul>
                           <li>
                              <a href="{{ route('user.logout') }}" class="border-bto2">
                                    <i class="flaticon-logout"></i>Log Out
                              </a>
                           </li>
                        </ul>
                  </div>
               </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-address contact-2">
                    <h3 class="heading-3">User Profile</h3>
            
                        <div class="row">
                            <div class="col-lg-12 ">
                                <div class="form-group name">
                                    <label>User Name</label>
                                    <input type="text" name="username" class="form-control" value="{{ Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group email">
                                    <label>Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}">
                                </div>
                            </div>
                            <div class="col-lg-12 ">
                                <div class="form-group subject">
                                    <label>Phone</label>
                                    <input type="text" name="phone" class="form-control" value="{{ Auth::user()->phone }}">
                                </div>
                            </div>
                            <div class="col-lg-12 ">
                                <div class="form-group number">
                                    <label>Address</label>
                                    <input type="text" name="address" class="form-control" value="{{ Auth::user()->address }}">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group message">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                </div>
                            </div>
                            <div class="row mb-3">
                                 <div class="col-sm-3">
                                 </div>
                                 <div class="col-sm-9">
                                    <img id="showImg" src="{{ (!empty(Auth::user()->image))?url('upload/userimg/'.Auth::user()->image):url('upload/noimg.png') }}" 
                                       alt="img" width="50px">
                                 </div>
                            </div>
                        
                       
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection