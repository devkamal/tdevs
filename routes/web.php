<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Middleware\RedirectIfAuthenticated;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    $id = Auth::user()->id;
    $userData = User::find($id);
    return view('frontend.userprofile.dashboard',compact('userData'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/user/logout', [ProfileController::class, 'UserLogout'])->name('user.logout');
});

require __DIR__.'/auth.php';

//Admin middleware
Route::middleware(['auth', 'role:admin'])->group(function () {
    //Admin Dashboard
    Route::controller(AdminController::class)->group(function () {
        Route::get('/admin/dashboard', 'AdminDashboard')->name('admin_dashboard');
        Route::get('/logout', 'AdminDestroy')->name('admin_logout');
        Route::get('/user-view','viewAllUser')->name('users.all');

        Route::get('/user-edit/{id}','editUser')->name('edit_user');
        Route::post('/update/{id}','update')->name('update_user');
        Route::get('/delete/{id}','destroy')->name('destroy_user');
    });

});  ////////End Admin middleware /////////


Route::get('/admin/login', [AdminController::class, 'index'])->name('login_form')
    ->middleware(RedirectIfAuthenticated::class);
